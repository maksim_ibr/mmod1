﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace MMod1
{
    [SuppressMessage("ReSharper", "CompareOfFloatsByEqualityOperator")]
    internal class Random
    {
        private long? _counter;
        private const int IterationsCount = 1000;

        private static long ToDecimal(string binaryNamber) => Convert.ToInt64(binaryNamber, 2);
        private static string ToBinary(long decimalNumber) => Convert.ToString(decimalNumber, 2);


        public long Next(int fromValue, int toValue)
        {
            while (true)
            {
                var result = (long) Math.Round(fromValue + (toValue - fromValue) * NextDouble3());
                if (result >= toValue || result <= fromValue)
                {
                    continue;
                }

                return result;
            }
        }

        public double[] NextPolar(double mean = 0, double deviation = 1)
        {
            double sValue;
            double uValue;
            double vValue;

            do
            {
                uValue = 2.0 * NextDouble3() - 1;
                vValue = 2.0 * NextDouble3() - 1;
                sValue = Math.Pow(uValue, 2) + Math.Pow(vValue, 2);
            } while (sValue > 1.0 || sValue == 0);

            var right = Math.Sqrt(-2 * Math.Log10(sValue) / sValue);
            var z0 = uValue * right;
            var z1 = vValue * right;
            return new[] { mean + z0 * deviation, mean + z1 * deviation };
        }

        // ReSharper disable once UnusedMember.Local
        private double NextDouble()
        {
            var tiks = DateTime.Now.Ticks.ToString();
            var startValue = ToBinary(long.Parse(tiks.Substring(tiks.Length - 7)));
            _counter = _counter ?? long.Parse(tiks.Substring(tiks.Length - 2));

            var startValueLength = startValue.Length;
            var oneQuarter = startValueLength / 4;

            var localCounter = 0;
            while (localCounter++ < IterationsCount)
            {
                var leftShiftBinary = startValue.Substring(oneQuarter) + startValue.Substring(0, oneQuarter);
                var rightShiftBinary = startValue.Substring(startValueLength - oneQuarter) +
                                       startValue.Substring(0, startValueLength - oneQuarter);

                startValue = ToBinary(ToDecimal(leftShiftBinary) + ToDecimal(rightShiftBinary));
                startValue = startValue.Length > startValueLength
                    ? startValue.Substring(0, startValueLength)
                    : startValue;
            }

            var result = ToDecimal(startValue).ToString();
            result = _counter++ % 10 == 0 ? $"0.0{result}" : $"0.{result}";
            return double.Parse(result);
        }

        private long _next = 7;
        private const long _mask = int.MaxValue;
        private const long _multiplier = 109;
        private double NextDouble3()
        {
            _next = (_multiplier * _next) % _mask;
            return (double)_next / _mask;
        }
    }
}
