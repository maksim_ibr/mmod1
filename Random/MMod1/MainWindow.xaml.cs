﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace MMod1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        private const int Count = 10000;
        private const int From = -15;
        private const int To = 15;

        private readonly ObservableCollection<KeyValuePair<int, long>> _uniform =
            new ObservableCollection<KeyValuePair<int, long>>();
        private readonly ObservableCollection<KeyValuePair<double, double>> _gauss =
            new ObservableCollection<KeyValuePair<double, double>>();

        public MainWindow()
        {
            InitializeComponent();

            GetRandomValues();
            ShowColumnCharts();
        }

        private void GetRandomValues()
        {
            var random = new Random();
            var result = new List<long>();
            var gaussResult = new List<double>();
            var columsDataB = new Dictionary<double, double>();
            var columsDataN = new Dictionary<double, double>();

            for (var i = 0; i < Count; ++i)
            {
                result.Add(random.Next(From, To));
                //gaussResult.Add(random.NextGaussian());
                gaussResult.AddRange(random.NextPolar());
            }

            for (var i = From + 1; i < To; ++i)
            {
                var column = new KeyValuePair<int, long>(i, result.Count(x => x == i));
                _uniform.Add(column);
                columsDataB.Add(column.Key, column.Value);
            }

            for (var i = -3.0; i < 3.2; i += 0.2)
            {
                var column = new KeyValuePair<double, double>(i, gaussResult.Count(x => x > i && x < i + 0.1));
                _gauss.Add(column);
                columsDataN.Add(column.Key, column.Value);
            }

            DMx.Text = $"{Assessments.MathematicalExpectation(columsDataB, Count):0.00}";
            DDx.Text = $"{Assessments.Dispertion(columsDataB, Count):0.00}";
            var confidenceIntervalM = Assessments.ConfidenceIntervalForMathematicalExpectation(columsDataB, Count);
            DIMx.Text = $"{confidenceIntervalM.Item1:0.00} < Mx < {confidenceIntervalM.Item2:0.00}";
            var confidenceIntervalD = Assessments.ConfidenceIntervalForDispertion(columsDataB, Count);
            DIDx.Text = $"{confidenceIntervalD.Item2:0.00} <= Dx < {confidenceIntervalD.Item1:0.00}";
            

            DMxN.Text = $"{Assessments.MathematicalExpectation(columsDataN, Count):0.00}";
            DDxN.Text = $"{Assessments.Dispertion(columsDataN, Count):0.00}";
            var confidenceIntervalMN = Assessments.ConfidenceIntervalForMathematicalExpectation(columsDataN, Count);
            DIMxN.Text = $"{confidenceIntervalMN.Item1:0.00} < Mx < {confidenceIntervalMN.Item2:0.00}";
            var confidenceIntervalDN = Assessments.ConfidenceIntervalForDispertion(columsDataN, Count);
            DIDxN.Text = $"{confidenceIntervalDN.Item2:0.00} <= Dx < {confidenceIntervalDN.Item1:0.00}";

            var criterionChiSquared = Assessments.CriterionChiSquared(columsDataB.Select(c => c.Value).ToList(), Count);
            var kolmogorov = Assessments.Kolmogorov(columsDataN, Count);

            Cor.Text = $"{Assessments.Correlation(result, 1):0.000}";
        }

        private void ShowColumnCharts()
        {
            lineChart.DataContext = _uniform;
            histogram.DataContext = _gauss;
        }
    }
}
