﻿using System;
using System.Collections.Generic;
using System.Linq;
using MathNet.Numerics.Distributions;

namespace MMod1
{
    public static class Assessments
    {
        private const double StudentValue10000 = 1.9604386;
        private const double Y = 0.95;
        private const double KolmogorovCoefficient = 1.3581;

        public static double MathematicalExpectation(Dictionary<double, double> samples, long count, bool square = false)
        {
            var result = 0.0;
            foreach (var sample in samples)
            {
                result += sample.Value / count * (square ? Math.Pow(sample.Key, 2) : sample.Key);
            }
        
            return result;
        }

        public static double Dispertion(Dictionary<double, double> samples, long count) =>
            MathematicalExpectation(samples, count, true) - Math.Pow(MathematicalExpectation(samples, count), 2);

        public static double DottedMathematicalExpectation(Dictionary<double, double> samples, long count) =>
            (double)count / (count - 1) * Dispertion(samples, count);

        public static Tuple<double, double> ConfidenceIntervalForMathematicalExpectation(Dictionary<double, double> samples,
            long count)
        {
            var s = Math.Sqrt(DottedMathematicalExpectation(samples, count));
            var m = MathematicalExpectation(samples, count);

            var value = s * StudentValue10000 / Math.Sqrt(count - 1);
            var left = m - value;
            var right = m + value;

            return new Tuple<double, double>(left, right);
        }

        public static Tuple<double, double> ConfidenceIntervalForDispertion(Dictionary<double, double> samples,
            long count)
        {
            var s = DottedMathematicalExpectation(samples, count);
            var left = count * s / ChiSquared.InvCDF(count - 1, (1 - Y) / 2);
            var right = count * s / ChiSquared.InvCDF(count - 1, (1 + Y) / 2);

            return new Tuple<double, double>(left, right);
        }

        public static bool CriterionChiSquared(ICollection<double> samples, long count)
        {
            var columnСount = samples.Count;
            var theoreticalQuantity = (double)count / columnСount;
            double result = 0;

            for (var i = 0; i < columnСount; i++)
            {
                result += Math.Pow(samples.ElementAt(i) - theoreticalQuantity, 2) / theoreticalQuantity;
            }

            return result < Math.Pow(ChiSquared.InvCDF(3, Y), 2);
        }

        public static bool Kolmogorov(Dictionary<double, double> samples, long count)
        {
            var funcE = new SortedDictionary<double, double>();
            var funcT = new SortedDictionary<double, double>();

            for (var i = -3.0; i < 3.2; i += 0.2)
            {
                funcE[i] = samples.Where(x => i > x.Key).Sum(x => x.Value) / count;
                funcT[i] = Normal.CDF(0, 1, i);
            }

            var maxDif = 0.0;
            for (var i = -3.0; i < 3.2; i += 0.2)
            {
                var tmp = Math.Abs(funcE[i] - funcT[i]);
                maxDif = tmp > maxDif ? tmp : maxDif;
            }

            var kolmValue = Math.Sqrt(count) * maxDif;

            return KolmogorovCoefficient > kolmValue;
        }

        public static double Correlation(ICollection<long> samples, int s)
        {
            var sum = 0.0;
            var index = 0;

            while (index + s < samples.Count)
            {
                sum += samples.ElementAt(index) * samples.ElementAt(index + s);
                index++;
            }

            var result = (12.0 / (samples.Count - s) * sum) - 3;

            return result;
        }
    }
}
